#include <stdio.h>

int main(int argc, char **argv) {
    int i = 0x006e6562;
    char *ptr = (char *)&i;
    printf("%s\n", ptr);

    ptr = (char *)argv;
    for (int i = 0; i< 8; i++) {
        printf("%x\n", ptr[i] & 0xff);
    }

    printf("%s\n", ptr);
    for (int i = 0; i < argc; i++) {
        printf("%d %p %p %s\n", i, argv, *argv, *argv);
        argv = argv+1;
    }
    printf("Hello, World!\n");
    return 0;
}