#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

double sum;
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
/*
 * add floating point numbers in the files passed on the commandline in
 * parallel. a thread is created for each commandline argument.
 */
void *addup(void *filename)
{
    double d;
    int fd = open((char*)filename, O_RDONLY);
    if (fd == -1) {
       perror((char*)filename);
       return NULL;
    }

    struct stat s;
    fstat(fd, &s);
    char *base = mmap(NULL, s.st_size, PROT_READ, MAP_SHARED, fd, 0);
    char *ptr = base;
    while (ptr < base+s.st_size) {
       pthread_mutex_lock(&lock);
       sum += strtod(ptr, &ptr);
       pthread_mutex_unlock(&lock);
       while (*ptr == '\n') ptr++;
    }
    munmap(base, s.st_size);
    close(fd);
    return NULL;
}

int main(int argc, char **argv)
{
    pthread_t *thread = malloc(sizeof(*thread)*(argc-1));
    for (int i = 1; i < argc; i++) {
	pthread_create(&thread[i-1], NULL, addup, argv[i]);
    }

    for (int i = 0; i < argc-1; i++) {
	pthread_join(thread[i], NULL);
    }

    printf("%lf\n", sum);
    return 0;
}
