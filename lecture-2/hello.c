// compile with gcc -o hello hello.c

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    for (int i = 0; i < argc; i++) {
        printf("%d %p %s\n", i, *(argv+i), *(argv+i));
    }
    char *name = argv[1];
    int count = strtol(argv[2], 0, 10);
    printf("count is %d\n", count);
    for (int i = 0; i < count; i++) {
        printf("Hello, %s!\n", name);
    }
}

