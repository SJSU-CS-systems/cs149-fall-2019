#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <pthread.h>

volatile double sum;
/*
 * add floating point numbers in the files passed on the commandline in
 * parallel. a thread is created for each commandline argument.
 */
void *addup(void *filename)
{
    double d;
    FILE *fh = fopen((char*)filename, "r");
    if (fh == NULL) {
       perror((char*)filename);
       return NULL;
    }
    while (fscanf(fh, "%lf", &d) == 1) {
       sum += d;
    }
    fclose(fh);
    return NULL;
}

int main(int argc, char **argv)
{
    pthread_t *thread = malloc(sizeof(*thread)*(argc-1));
    for (int i = 1; i < argc; i++) {
	pthread_create(&thread[i-1], NULL, addup, argv[i]);
    }

    for (int i = 0; i < argc-1; i++) {
	pthread_join(thread[i], NULL);
    }

    printf("%lf\n", sum);
    return 0;
}
