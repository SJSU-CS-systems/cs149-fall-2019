#include <stdio.h>

int main() {
    int count = 0;
    fprintf(stdout, "how many times? ");
    fflush(stdout);
    scanf("%d", &count);
    for (int i = 0; i < count; i++) {
        printf("Hello, World!\n");
    }
    return 0;
}
