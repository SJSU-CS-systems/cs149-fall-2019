#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct rec {
    struct rec *next;
    int id;
    char *name;
};

void add_to_list(struct rec **head_ptr, int id, char *name) {
    struct rec *node = malloc(sizeof(struct rec));
    node->id = id;
    node->name = name;
    node->next = *head_ptr;
    *head_ptr = node;
}

void print_list(struct rec *head) {
    while(head != NULL) {
        printf("%d %s\n", head->id, head->name);
        head = head->next;
    }
}

int main() {
    struct rec *head = NULL;
    printf("%d head = %p\n", __LINE__, head);
    add_to_list(&head, 1, "ben");
    printf("%d head = %p\n", __LINE__, head);
    add_to_list(&head, 2, "amy");
    printf("%d head = %p\n", __LINE__, head);
    add_to_list(&head, 0, "sam");
    printf("%d head = %p\n", __LINE__, head);
    print_list(head);
    return 0;
}
