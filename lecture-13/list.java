public class list {
    static class Rec {
        Rec next;
        int id;
        String name;
    }

    static Rec add_to_list(Rec head, int id, String name) {
        Rec new_node = new Rec();
        new_node.id = id;
        new_node.name = name;
        new_node.next = head;
        return new_node;
    }

    static void print_list(Rec current) {
        while(current != null) {
            System.out.printf("%d %s\n", current.id, current.name);
            current = current.next;
        }
    }

    public static void main(String a[]) {
        Rec head = null;
        head = add_to_list(head, 1, "ben");
        head = add_to_list(head, 2, "amy");
        head = add_to_list(head, 0, "sam");
        print_list(head);
    }
}
