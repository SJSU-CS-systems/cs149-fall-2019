#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv) {
    char name[256];
    int i = 1;
    int *ptr = &i;
    sprintf(name, "ben%s", argv[1]);
    printf("Hello, %s! %p\n", name, name);
    pid_t pid = fork();
    if (pid == 0) {
        sprintf(name, "i am the child");
        *ptr = 32;
        fork();
        i++;
        printf("after fork %d\n", i);
    } else if (pid == -1) {
        sprintf(name, "error happened");
    } else {
        sprintf(name, "i'm the parent the child is %d", pid);
        *ptr = 48;
    }
    //sleep(10);
    printf("%s still at %p\n", name, name);
    printf("my i is %d\n", i);
    sleep(1);
    return 0;
}