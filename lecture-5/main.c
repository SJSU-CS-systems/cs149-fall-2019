#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char **argv) {
    char name[256];
    int i = 1;
    int *ptr = &i;
    sprintf(name, "ben%s", argv[1]);
    printf("Hello, %s! %p\n", name, name);
    pid_t pid = fork();
    if (pid == 0) {
        sprintf(name, "i am the child");
        *ptr = 32;
        i++;
        printf("after fork %d\n", i);
    } else if (pid == -1) {
        sprintf(name, "error happened");
    } else {
        int stat;
        int *rc = &stat;
        pid_t p;
        sleep(20);
        p = wait(rc);
        printf("wait returned %d %d\n", p, stat);
        sprintf(name, "i'm the parent the child is %d", pid);
        *ptr = 48;
    }
    //sleep(10);
    printf("%s still at %p\n", name, name);
    printf("my i is %d\n", i);
    return 0;
}