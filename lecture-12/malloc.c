#include <stdio.h>
#include <stdlib.h>

int main()
{
	int bytes_to_allocate = 1;
	for (int i = 0; i < 10; i++) {
		printf("allocating %d bytes\n", bytes_to_allocate);
		for (int j = 0; j < 4; j++) {
		    char *ptr = malloc(bytes_to_allocate);
		    printf("%p\n", ptr);
		}
		bytes_to_allocate *= 4;
	}
}
