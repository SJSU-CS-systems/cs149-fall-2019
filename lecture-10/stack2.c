#include <stdio.h>
 
int foo(int b) {
    	int a = b + 2;
    	printf("%p\n", &a);
    	return a;
}

int main() {
    	int stack_start = 3;
    	printf("%p\n", &stack_start);
    	foo(3);
    	return 0;
}

