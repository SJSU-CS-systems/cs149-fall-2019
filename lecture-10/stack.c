#include <stdio.h>
#include <stdlib.h>

int foo(int b) {
	int a = 2;
	char *ptr = malloc(1024);
	printf("address of a = %p b = %p ptr = %p allocated memory %p\n", &a, &b, &ptr, ptr);
	return a+b;
}

int main() {
	int a = 3;
	printf("address of a is %p\n", &a);
	foo(6);
}
