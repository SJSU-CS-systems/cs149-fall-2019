#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

sem_t sem;
int race_started = 0;

void *run_thread(void *v) {
   long racer = (long)v;
   printf("racer %ld READY!\n", racer);
   sem_wait(&sem);
   printf("racer %ld RUNNING\n", racer);
   sleep(2);
   printf("racer %ld FINISHED\n", racer);
   sem_post(&sem);
}

int main() {
   pthread_t ts[4];

   sem_init(&sem, 0, 0);

   printf("READY\n");
   for (long t = 0; t < 4; t++) {
       pthread_create(&ts[t], NULL, run_thread, (void*)t);
   }
   sleep(1);
   printf("SET\n");
   sleep(1);
   printf("GO\n");
   sem_post(&sem);
   sem_post(&sem);
   for (int t = 0; t < 4; t++) {
       pthread_join(ts[t], NULL);
   }
   printf("all done!\n");
}
