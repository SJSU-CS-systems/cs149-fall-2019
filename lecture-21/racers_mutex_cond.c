#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

pthread_mutex_t lock;
pthread_cond_t cond;
int race_started = 0;

void *run_thread(void *v) {
   long racer = (long)v;
   printf("racer %ld READY!\n", racer);
   pthread_mutex_lock(&lock);
       while (!race_started) {
           pthread_cond_wait(&cond, &lock);
       }
   pthread_mutex_unlock(&lock);
   printf("racer %ld RUNNING\n", racer);
   sleep(5);
   printf("racer %ld FINISHED\n", racer);
}

int main() {
   pthread_t ts[4];

   pthread_mutex_init(&lock, NULL);
   pthread_cond_init(&cond, NULL);

   printf("READY\n");
   for (long t = 0; t < 4; t++) {
       pthread_create(&ts[t], NULL, run_thread, (void*)t);
   }
   sleep(1);
   printf("SET\n");
   sleep(10);
   pthread_mutex_lock(&lock);
   race_started = 1;
   pthread_cond_broadcast(&cond);
   printf("GO!\n");
   pthread_mutex_unlock(&lock);
   for (int t = 0; t < 4; t++) {
       pthread_join(ts[t], NULL);
   }
   printf("all done!\n");
}
