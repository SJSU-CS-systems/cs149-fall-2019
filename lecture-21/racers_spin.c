#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

pthread_spinlock_t lock;
int race_started = 0;

void *run_thread(void *v) {
   long racer = (long)v;
   printf("racer %ld READY!\n", racer);
   int go = 0;
   while (!go) {
       pthread_spin_lock(&lock);
       go = race_started;
       pthread_spin_unlock(&lock);
   }
   printf("racer %ld RUNNING\n", racer);
   sleep(2);
   printf("racer %ld FINISHED\n", racer);
}

int main() {
   pthread_t ts[4];

   pthread_spin_init(&lock, 0);

   printf("READY\n");
   for (long t = 0; t < 4; t++) {
       pthread_create(&ts[t], NULL, run_thread, (void*)t);
   }
   sleep(1);
   printf("SET\n");
   sleep(1);
   pthread_spin_lock(&lock);
   race_started = 1;
   printf("GO\n");
   pthread_spin_unlock(&lock);
   for (int t = 0; t < 4; t++) {
       pthread_join(ts[t], NULL);
   }
   printf("all done!\n");
}
