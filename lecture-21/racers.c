#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void *run_thread(void *v) {
   long racer = (long)v;
   printf("racer %ld READY!\n", racer);
   printf("racer %ld RUNNING\n", racer);
   sleep(2);
   printf("racer %ld FINISHED\n", racer);
}

int main() {
   pthread_t ts[4];

   printf("READY\n");
   for (long t = 0; t < 4; t++) {
       pthread_create(&ts[t], NULL, run_thread, (void*)t);
   }
   sleep(1);
   printf("SET\n");
   sleep(1);
   printf("GO\n");
   for (int t = 0; t < 4; t++) {
       pthread_join(ts[t], NULL);
   }
   printf("all done!\n");
}
