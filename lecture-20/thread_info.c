#include <stdio.h>
#include <pthread.h>

struct run_info { int count; int thread; };

void *run_thread(void *v) {
   struct run_info *info = v;
   printf("thread %d\n", info->thread);
}

int main() {
   pthread_t ts[4];
   struct run_info info = {.count = 4 };

   for (int t = 0; t < 4; t++) {
       info.thread = t;
       pthread_create(&ts[t], NULL, run_thread, &info);
   }
   sleep(1);
}
