#include <stdio.h>
#include <pthread.h>
#include <threads.h>

struct run_info { int count; int thread; };

thread_local int counter;
void increment_counter(int count) {
   for (int i = 0; i < count; i++) counter++;
}
void *run_thread(void *v) {
   struct run_info *info = v;
   increment_counter(info->count);
   return (void*)(long)counter;
}
int main () {
    struct run_info info = {.count = 1000 };
    pthread_t ts[4];
    for (int t = 0; t < 4; t++) {
       pthread_create(&ts[t], NULL, run_thread, &info);
    }
   for (int t = 0; t < 4; t++) {
        void *v;
        pthread_join(ts[t], &v);
        printf("%d\n", (int)(long)v);
    } }

