#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct rec *head = NULL;
struct rec {
    struct rec *next;
    int id;
    char *name;
};

void add_to_list(struct rec **head_ptr, int id, char *name) {
    struct rec *node = malloc(sizeof(struct rec));
    node->id = id;
    node->name = name;
    node->next = *head_ptr;
    *head_ptr = node;
}

void print_list(struct rec *head) {
    while(head != NULL) {
        printf("%d %s\n", head->id, head->name);
        head = head->next;
    }
}

void *add_to_list(void *v) {
}

int main() {
    pthread_t p[10];

    for (int i = 0; i < 10; i++) {
        pthread_create(&p[i], NULL, add_to_list, (void *)i);
    }

    for (int i = 0; i < 10; i++) {
	pthread_join(p[i], NULL);
    }
    print_list(head);
    return 0;
}
