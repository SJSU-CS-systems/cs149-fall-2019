#include <stdio.h>

struct someChars {
	char i;
	short j;
	char k;
	char l;
};

int main() {
	struct someChars cs;
	printf("cs @ %p\n", &cs);
	printf("cs.i @ %p\n", &cs.i);
	printf("cs.j @ %p\n", &cs.j);
	printf("cs.k @ %p\n", &cs.k);
	printf("cs.k @ %p\n", &cs.l);
	return 0;
}
