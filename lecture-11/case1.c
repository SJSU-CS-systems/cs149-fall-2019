#include <stdio.h>
#include <string.h>

struct Student {
    int id;
    char name[20];
};

char *getName(struct Student *s) {
    return s->name;
}

int main() {
    struct Student s = { 1, "fred"};

    printf("s & %p\n", &s);

    char *name = getName(&s);

    printf("name @ %p\n", name);
    printf("name is %s\n", name);
    return 0;
}
