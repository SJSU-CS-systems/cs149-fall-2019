#include <stdio.h>

struct someInts {
	int i;
	int j;
	int k;
};

int main() {
	struct someInts is;
	printf("is @ %p\n", &is);
	printf("is.i @ %p\n", &is.i);
	printf("is.j @ %p\n", &is.j);
	printf("is.k @ %p\n", &is.k);
	return 0;
}
