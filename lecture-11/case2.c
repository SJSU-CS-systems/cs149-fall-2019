#include <stdio.h>
#include <string.h>

struct Student {
    int id;
    char name[20];
};

char *getName(struct Student s) {
    return s.name;
}

int main() {
    struct Student fred  = { 1, "fred"};
    struct Student joe = { 2, "joe"};

    char *fred_name = getName(fred);

    printf("fred_name @ %p\n", fred_name);
    printf("fred_name is %s\n", fred_name);
    return 0;
}
